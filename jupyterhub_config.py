"""
Example for a Spawner.pre_spawn_hook
create a directory for the user before the spawner starts
"""
# pylint: disable=import-error
import os
import shutil
import nativeauthenticator
from jupyter_client.localinterfaces import public_ips

c.JupyterHub.tornado_settings = {'headers': {'Content-Security-Policy': "frame-ancestors * 'self' "}, 'slow_spawn_timeout': 30}
c.JupyterHub.allow_origin = '*'
c.JupyterHub.base_url = '/jupyter-hub'
c.JupyterHub.shutdown_on_logout = True

# Use the DockerSpawner to serve your users' notebooks

c.Spawner.args = [ '--config=/home/admin/jupyter_notebook_config.py']
c.DockerSpawner.cmd=["jupyter-labhub"]


c.JupyterHub.authenticator_class = "nativeauthenticator.NativeAuthenticator"
c.Authenticator.admin_users = {"admin"}
c.LocalAuthenticator.create_system_users = True

if not isinstance(c.JupyterHub.template_paths, list):
    c.JupyterHub.template_paths = []
c.JupyterHub.template_paths.append(
    f"{os.path.dirname(nativeauthenticator.__file__)}/templates/"
)

c.NativeAuthenticator.check_common_password = False
c.NativeAuthenticator.minimum_password_length = 4
c.NativeAuthenticator.allowed_failed_logins = 0
c.NativeAuthenticator.seconds_before_next_try = 600

c.NativeAuthenticator.enable_signup = True
c.NativeAuthenticator.open_signup = True
c.NativeAuthenticator.ask_email_on_signup = False

c.NativeAuthenticator.allow_2fa = False

c.NativeAuthenticator.import_from_firstuse = False
c.NativeAuthenticator.firstuse_dbm_path = "/home/user/passwords.dbm"
c.NativeAuthenticator.delete_firstuse_db_after_import = False


def pre_spawn_hook(spawner):
    username = spawner.user.name
    try:
        import pwd
        pwd.getpwnam(username)
    except KeyError:
        import subprocess
        subprocess.check_call(['useradd', '-ms', '/bin/bash', username])

        shutil.copyfile('/home/admin/ASD.ipynb', '/home/'+ username + '/ASD.ipynb')
        shutil.copyfile('/home/admin/Dashboard.ipynb', '/home/'+ username + '/Dashboard.ipynb')
        shutil.copyfile('/home/admin/DECODER_API.py', '/home/'+ username + '/DECODER_API.py')
        shutil.copytree('/home/admin/tbcnn', '/home/'+ username + '/tbcnn')
        
        subprocess.check_call(['chown',username, '/home/'+ username + '/ASD.ipynb'])
        subprocess.check_call(['chown',username ,'/home/'+ username + '/Dashboard.ipynb'])
        subprocess.check_call(['chown',username ,'/home/'+ username + '/DECODER_API.py'])
        subprocess.check_call(['chown',username ,'/home/'+ username + '/tbcnn'])


def clean_dir_hook(spawner):
    """Delete directory"""
    username = spawner.user.name  # get the username
    temp_path = os.path.join('/volumes/jupyterhub', username, 'temp')
    if os.path.exists(temp_path) and os.path.isdir(temp_path):
        shutil.rmtree(temp_path)

c.Spawner.pre_spawn_hook = pre_spawn_hook
c.Spawner.post_stop_hook = clean_dir_hook


c.JupyterHub.admin_access = True
c.Authenticator.admin_users = {'admin'}

c.JupyterHub.api_tokens = {
    '9d22c1b9f3b34449b125de361f86267a': 'admin',
}

