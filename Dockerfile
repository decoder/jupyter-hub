FROM jupyterhub/jupyterhub:latest

ENV no_proxy=localhost,127.0.0.1,*.ourcompany.fr,10.*,localdomain,cluster.local
ENV NO_PROXY=localhost,127.0.0.1,*.ourcompany.fr,10.*,localdomain,cluster.local

COPY ./jupyterhub_config.py /etc/jupyterhub/jupyterhub_config.py

RUN pip install ipykernel --upgrade

RUN python3 -m pip install jupyterlab 
RUN jupyter serverextension enable --py jupyterlab --sys-prefix
RUN python3 -m pip install jupyterhub-nativeauthenticator
RUN python3 -m pip install markdown
RUN python3 -m pip install md-mermaid
RUN python3 -m pip install Pillow
RUN python3 -m pip install matplotlib
RUN python3 -m pip install pymongo==3.6.0
RUN python3 -m pip install json2html
RUN python3 -m pip install ipywidgets
RUN python3 -m pip install plotly


# Install Data Dons Discern dependencies
# RUN pip install -r /home/jovyan/examples/tbcnn/requirements.txt
# RUN conda env update --name myenv1 --file /home/jovyan/tbcnn/dependencies.yml

RUN python3 -m pip install GitPython==3.1.17
RUN python3 -m pip install Werkzeug==1.0.1
RUN python3 -m pip install zipfile38==0.0.3
RUN python3 -m pip install scipy==1.6.2
RUN python3 -m pip install pytest==6.2.3
RUN python3 -m pip install pandas==1.2.4
RUN python3 -m pip install matplotlib==3.3.4
RUN python3 -m pip install numpy==1.20.0
RUN python3 -m pip install gensim==4.0.1
RUN python3 -m pip install torch==1.8.1


RUN useradd -d /home/admin -s /bin/bash admin -p "$(openssl passwd -1 admin)"

RUN  mkdir /home/admin
COPY ./notebooks/admin/artifacts_by_project.ipynb			/home/admin/examples/artifacts_by_project.ipynb
COPY ./notebooks/admin/artifacts_in_project.ipynb 			/home/admin/examples/artifacts_in_project.ipynb
COPY ./notebooks/admin/decoderpkm.py 						/home/admin/examples/decoderpkm.py
COPY ./notebooks/admin/epicsqueries.py 						/home/admin/examples/epicsqueries.py
COPY ./notebooks/admin/nameinput.py 						/home/admin/examples/nameinput.py
COPY ./notebooks/admin/num_art_by_project.ipynb				/home/admin/examples/num_art_by_project.ipynb
COPY ./notebooks/admin/percentage_art_in_project.ipynb 		/home/admin/examples/percentage_art_in_project.ipynb
COPY ./notebooks/admin/pymongo_config.json					/home/admin/examples/pymongo_config.json
COPY ./notebooks/admin/requirementsqueries.py				/home/admin/examples/requirementsqueries.py
COPY ./notebooks/admin/test.ipynb							/home/admin/examples/test.ipynb

COPY ./notebooks/admin/ASD_admin.ipynb 						/home/admin/ASD_admin.ipynb
COPY ./notebooks/admin/widget_funcs.py						/home/admin/widget_funcs.py

COPY ./notebooks/DECODER_API.py								/home/admin/DECODER_API.py
COPY ./notebooks/ASD.ipynb									/home/admin/ASD.ipynb
COPY ./notebooks/Dashboard.ipynb							/home/admin/Dashboard.ipynb

COPY ./notebooks/tbcnn                 				    	/home/admin/tbcnn

RUN chown -R admin:admin /home/admin

COPY ./jupyter_notebook_config.py /home/admin/jupyter_notebook_config.py


EXPOSE 8070
EXPOSE 8081

CMD ["jupyterhub", "--ip", "0.0.0.0", "--port", "8070", "-f", "/etc/jupyterhub/jupyterhub_config.py"]