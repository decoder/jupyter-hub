DECODER JupyterHub
========================

This application is a product of the H2020 DECODER project.

It connects to the DECODER PKM, through the DECODER PKM API, to generate living documents describing the information inside DECODER in different and flexible manners.

This application is designed to run bundled inside the DECODER Frontend application. That is, it is a web application that, although independent, is bundled inside the DECODER GUI.

# Current capabilities

This web application allows users to run Phyton code inside the browser, and to process data contained into the PKM to generate graphics and any other calcullations desired.

This application has pre-configured Notebooks, that extract information of the PKM and render some graphics and interesting insights. 

The Notebooks provided can be dinamically modified by users, to extract different information. New Notebooks or Phyton files can be included also.

# Running the application

Several options to run the application do exist. The preferred one uses Docker-compose.

To locally execute this software, run:

```docker-compose jupyter-hub pull 
> docker-compose up -d jupyter-hub
```

# Building the application

To build the application check out the code and do a typical Docker build command:

```
> docker build decoder-jupyter-hub:latest .
```

# Usage

Access to DECODER Jupyter-hub is password protected.
After a first installation, users must be created. To do so, open the login page and press the "Registrer user" button. From here you can create users as desired. 
The user admin will automatically have admin priviliges in the platform.

## License & copyright
Licensed under the [[Apache Version 2.0](https://gitlab.ow2.org/decoder/jupyter-hub/-/blob/master/LICENSE)]
