import requests, io
import getpass
import re
from IPython import get_ipython
from IPython.display import display, HTML, Javascript
import base64
from PIL import Image
import matplotlib.pyplot as plt

header=''
project=''
host='https://decoder-tool.ow2.org'

def getSessionHeader():
    global header
    return {'Key' : header}

def setSessionHeader(var):
    global header
    header = var
    return

def setHost(var):
    global host
    host = var
    return 

def extractSessionHeader():
    script = """
    setTimeout(() => {
    document.querySelectorAll('[type=password]')[0].value=localStorage.getItem('access_token');
    this.dispatchEvent(new KeyboardEvent('keydown', {key:'enter', keyCode: 13, metaKey: false}));
    },1000)
    """
    display(HTML((
        '<img src onerror="{}" style="display:none">'
    ).format(script)))

    password = getpass.getpass('Press intro: to log in:')

    setSessionHeader(password)

def login():
    return

def getProjects():
    header = getSessionHeader()
    response = requests.get(host +"/pkm/user/admin/project" , headers=header)
    return response.json()

def selectProject(var):
    global project
    project = var
    return

def getCurrentProject():
    global project
    if project =='': 
        project = getpass.getuser()
    return project

def getAllLogs():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/log/" + project, headers=header)
    return response.json()
    
def getLogsByTool(tool):
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/log/" + project, headers=header)
    rspJsonLogs= response.json()
    return list(filter(lambda x: x['tool'] == tool ,rspJsonLogs))

def getAllDocuments():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/documents/" + project, headers=header)
    return response.json()
     
def getDocumentContents(file):
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/doc/rawDoc/" + project + "/" + file, headers=header)
    return response.json() 

def getAllFiles():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/files/" + project, headers=header)
    return response.json()

def getOneFile():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/files/" + project, headers=header)
    return response.json()[:1]

def getAllSourceCodes():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/code/" + project, headers=header)
    return response.json() 

def getSourceCodeByName(source):
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/code/" + project, headers=header)
    filter(lambda x: x['rel_path'].contains(source), response.json())
    return response.json() 

def getAllAnnotations():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/code/annotations/" + project, headers=header)
    return response.json() 

def getAllComments():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/code/comments/" + project, headers=header)
    return response.json() 

def getCommentsInFile(source):
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/code/comments/" + project, headers=header)
    filter(lambda x: x['sourceFile'].contains(source), response.json())
    return response.json() 

def getAllUML():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/uml/uml_class/" + project, headers=header)
    return response.json() 

def getAllStateModel():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/testar/state_model/" + project, headers=header)
    return response.json() 

def getAllRelations():
    project = getCurrentProject()
    header = getSessionHeader()
    response = requests.get(host +"/pkm/traceability_matrix/" + project, headers=header)
    return response.json() 
 
def getAllUserStories(filename):
    project = getCurrentProject()
    header = getSessionHeader()
    response = getDocumentContents(filename)
    # display(response)
    markdownData = response['content']
  #  regexUs= re.findall("(As )(an|a)(.*)(I would like to|I want to)(.*)(so|so that|in order to|\. )(.|\n)*(AC|Details|Acceptance criteria)((.|\n)*)(==|\n\n)", markdownData)  
    return markdownData 

def getAllEpics(filename):
    project = getCurrentProject()
    header = getSessionHeader()
    response = getDocumentContents(filename)
    markdownData = response['content']
    #regexEpics = re.findall("(Epic:)(.*)(\n)", markdownData)
    return markdownData 
    
def getProjectReadMe():
    project = getCurrentProject()
    header = getSessionHeader()
    response = getAllFiles()
    for entry in response:
        if 'README' in entry['rel_path']:
            return entry['content']
    return 

def getLogsForCodeSummarizationTool():
    return getLogsByTool('code_summarization')

def getLogsForJMLGen():
    return getLogsByTool('jmlgen')

def getLogsForJavaParser():
    return getLogsByTool('java_parser')

def getNumberOfJMLGenAditions():
    jmlgenLogs=getLogsForJMLGen()
    countJML = 0
    for entry in jmlgenLogs:
        for msg in entry['messages']:
            if "line" in msg: 
                countJML = countJML + msg.count('line')
    return countJML

def getNumberOfAnnotations():
    annotations=getAllAnnotations()
    count = 0
    for entry in annotations:
        for ann in entry['annotations']:
            count = count + len(ann)
    return count

def getNumberOfCodeSummarizationAdditions():
    comments=getAllComments()
    count = 0
    for entry in comments:
        for msg in entry['comments']:
            for comment in msg['comments']:
                if "DECODER-CODE-SUMMARIZATION" in comment: 
                    count = count + comment.count('DECODER-CODE-SUMMARIZATION')
    return count

def getNumberOfRelationsAdded():
    relations=getAllRelations()   
    return len(relations)

def getNumberOfUMLAdditions():
    umlClass=getAllUML()   
    return len(umlClass)

def getNumberOfStateModelCalculations():
    stateModels=getAllStateModel()   
    return len(stateModels)

def getNumberOfUserStoriesParsed():
    stateModels=getAllStateModel()   
    return len(stateModels)

def getNumberOfEpicsParsed(filename):
    regexEpics = re.findall("(Epic:)(.*)(\n)", getAllEpics(filename))
    return len(regexEpics)

def getNumberOfUserStoriesParsed(filename):
    markdownData = getAllUserStories(filename) 
    markdownUS = markdownData.split("US:")
    return len(markdownUS)

def getNumberOfComments():
    comments=getAllComments()
    count = 0
    for entry in comments:
        for msg in entry['comments']:
            count = count + len(msg)
    return count

def getNumberOFDocuments():
    return len(getAllDocuments())    

def generateUSGraph(user, iwant, soI, ac)-> str:
    text5 = soI
    if len(text5)>50:
        text5= text5[:50] + "<br>" + text5[50:]
    return """flowchart LR
        id1((fa:fa-user <br>"""+user+"""))-->id2("""+iwant+"""<br> """ +text5+ """<br> """ + ac + """)
        style id1 stroke:#fff,fill:#fff, stroke-width:4px, padding :2px
        style id2 fill:#bbf,stroke:#666,stroke-width:2px,color:#000"""

def getPicturesWithName(var):
    searched = []    
    project = getCurrentProject()
    header = getSessionHeader()
    baseImageUrL = host +"/pkm/doc/rawdoc/" + project + "/"
    
    documents=getAllFiles()

    for entry in documents:
        # display(entry)
        if var in entry['rel_path'] and 'image' in entry['mime_type']:
            imageURL = baseImageUrL + entry['rel_path']
            response = requests.get(imageURL, headers=header).json()
            searched.append(response['content'])
    return searched

def printMermaidList(graphList:list)-> None:
    count = 0;
    for graph in graphList:
        graphbytes = graph.encode("ascii")
        base64_bytes = base64.b64encode(graphbytes)
        base64_string = base64_bytes.decode("ascii")
        img = Image.open(io.BytesIO(requests.get('https://mermaid.ink/img/' + base64_string).content))
        ax=plt.gca()
        ax.get_xaxis().set_visible(False)
        ax.get_yaxis().set_visible(False)
        plt.rcParams['figure.figsize'] = [15, 15]
        plt.imshow(img)
        plt.figure()
        count = count +1
   
