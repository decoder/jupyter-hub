import ipywidgets as widgets
from DECODER_API import getProjects


def show_project_widget():
   
    projects = getProjects()
    projectNames = []
    
    for project in projects:
        projectNames.append(project['name'])

    projectW = widgets.Dropdown(
        options=projectNames,
        value='mythaistar',
        description='Project:',
        disabled=False,
    )

    return projectW