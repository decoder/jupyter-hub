from decoderpkm import GetConn, GetConf
from IPython.display import HTML, display
from bson.json_util import dumps
import pymongo
from pymongo import TEXT
import json
from json2html import *
conn = GetConn()
conf = GetConf()

db = conn[conf['db']]

sourceCodeCollection = db.Sourcecode
traceCollection = db.TraceabilityMatrix
sourceCodeCollection.create_index(
    [('fileName', TEXT)], default_language='english')


def GetAllEpics():
    # Get all entries
    entries = sourceCodeCollection.find({"$text": {"$search": "epic"}})

    # Loop looks for the related arifacts on entries that are US
    for entry in entries:
        userStories = []
        id = entry._id
        relatedArts = traceCollection.find({"ref1": {"artefact": id}})
        for related in relatedArts:
            idre = related.ref2.artefact
            us = sourceCodeCollection.find_one({"_id": idre}, {
                                               "$text": {"$search": "userstory \"user story\" us"}}).sort({"fileName": 1})

            userStories.append(us)

    # Printing the results
        epics_in_html = ''
        us_in_html = ''
        epics_in_html = json2html.convert(json=entry)
        us_in_html = json2html.convert(json=userStories)
        display(HTML(epics_in_html))
        display(HTML(us_in_html))
    # print(entry)
    # print(userStories)


def GetNumberOfGlobalTypes():

    entries = sourceCodeCollection.find({"$text": {"$search": "epic"}})
    dictTable = {}
    uniqueGlobals = []

    # Get a dictionary with the number of global types for each project
    for entry in entries:
        dictTable[entry['fileName']] = {}
        gTypes = entry['globals']

        for gType in gTypes:
            if list(gType.keys())[0] in dictTable[entry['fileName']].keys():
                dictTable[entry['fileName']][list(gType.keys())[0]] += 1
            else:
                dictTable[entry['fileName']][list(gType.keys())[0]] = 1
            if not list(gType.keys())[0] in uniqueGlobals:
                uniqueGlobals.append(list(gType.keys())[0])

    # Create HTML code for the table
    tableHtml = "<table><tr><th>Path</th>"
    for u in uniqueGlobals:
        tableHtml += "<th>"+u+"</th>"
    tableHtml += "</tr>"

    for key, value in dictTable.items():
        tableHtml += "<tr><td>"+key+"</td>"
        for u in uniqueGlobals:
            tableHtml += "<td>"+str(value.get(u))+"</td>"
        tableHtml += "</tr>"

    # Draw actual table with IPython
    display(HTML(tableHtml))


def GetEpicByName(name=str):
    my_search_format = "\"{}\""
    my_search = my_search_format.format(name)
    epic = sourceCodeCollection.find_one({"$text": {"$search": my_search}})

    # Looking for user stories related to the epic
    userStories = []
    id = epic._id
    relatedArts = traceCollection.find({"ref1": {"artefact": id}})
    for related in relatedArts:
        idre = related.ref2.artefact
        us = sourceCodeCollection.find_one({"_id": idre}, {
                                           "$text": {"$search": "userstory \"user story\" us"}}).sort({"fileName": 1})
        if us is not None:
            userStories.append(us)

    # Printing the results in an HTML table
    epics_in_html = ''
    us_in_html = ''
    epics_in_html = json2html.convert(json=epic)
    us_in_html = json2html.convert(json=userStories)
    display(HTML(epics_in_html))
    display(HTML(us_in_html))

def DrwaUS(name=str):
    graph = """
    flowchart LR
        id1((fa:fa-user <br> User ))-->id2(Do those things<br/><br>AC:<br>Acceptance 1<br/>Acceptance 2<br/>Acceptance 3)
        style id1 stroke:#fff,fill:#fff, stroke-width:4px, padding :2px
        style id2 fill:#bbf,stroke:#666,stroke-width:2px,color:#000
    """

    graphbytes = graph.encode("ascii")
    base64_bytes = base64.b64encode(graphbytes)
    base64_string = base64_bytes.decode("ascii")
    img = Image.open(io.BytesIO(requests.get('https://mermaid.ink/img/' + base64_string).content))
    plt.imshow(img)

def GetUSByName(name=str):
    DrwaUS(str)
    my_search_format = "\"{}\""
    my_search = my_search_format.format(name)
    userStories = sourceCodeCollection.find_one(
        {"$text": {"$search": my_search}})

 # Looking for user stories related to the epic
 #   userStoriesArray = []
 #   id = userStories._id
 #   for us in userStories:
 #       us = sourceCodeCollection.find_one({"_id": id}, {
 #                                          "$text": {"$search": "userstory \"user story\" us"}}).sort({"fileName": 1})
 #       if us is not None:
 #           userStoriesArray.append(us)
 #
 # Printing the results in an HTML table
    us_in_html = ''
    us_in_html = json2html.convert(json=userStories)
    display(HTML(us_in_html))


